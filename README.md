# Some of my Notion Notes I'd like to share

A quote from one of my favorite programmer (also a streamer)'s [blog](https://robs.io/) to start this repo.

> So, I guess I’m just taking a moment to realize what matters most, and enjoying the silent comradery with amazing engineers, all of us “building civilization” together in our own quiet workshops, taking an occasional glance at the Trump/Kim Kardashian madness of the popular world, and then quietly smiling and turning back to our work like Mr. Horologist adjusting his glasses as he looks back at his watches.
> 
> There, I feel better.

This repo mainly contains some notes I took while reading some books or some thoughts that I'd love to share over the internet. 

Sorry if you find any mistakes or typos from my writings and feel free to open a issue / make a pull request. Just hope everyone have a great day. 😆

## Index of my notes

### System Design

- [ElasticSearch. An intro](es_an_into/es_an_intro.md) [English version incomings]
  - from book [Elasticsearch: The Definitive Guide](https://www.oreilly.com/library/view/elasticsearch-the-definitive/9781449358532/) 
  - 10/10 recommanded. Detailed and straighforward. One of the best books I have read in 2020.